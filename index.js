var params = require('./params');

var utils = require('./utils');

// modules

var Request = require('request');
var Async = require('async');
var ø = require('validator');
var _ = require('underscore');

/////////////////
// Constructor //
/////////////////

/**
 * Constructor of CheckpointAnalyser, needs to be supplied the courier and options if available
 * @param {Number} user
 * @param {String} token
 */
function API(user, token) {
  if (!(ø.isInt(user) && token.length == 30))
    return new Error('Invalid user/ token combination');

  this.user = user;
  this.token = token;

}

//////////////////////
// Public Functions //
//////////////////////

/**
 * creates a new tracking on the parcelLab API
 * @param  {Object}   payload        specifies the tracking to be created
 * @param  {Function} callback       (error, result)
 */
API.prototype.createTracking = function(payload, callback) {
  var user_id = this.user;
  var token = this.token;

  checkPayload(payload, function(err, isValidPayload) {

    if (isValidPayload) {

      var payloads = multiplyOnTrackingNumber(payload);
      Async.eachSeries(payloads, function(p, callback) {

        postTrackingToParcelLabAPI(p, user_id, token, function(err, res) {
          callback(err);
        });

      }, function(err) {
        if (err) callback(new Error(err), {
          result: 'error',
          message: 'Tracking to be created is invalid'
        });
        else callback(null, {
          result: 'success',
          message: 'All trackings posted'
        });
      });

    } else callback('Nothing posted', {
      result: 'error',
      message: err
    });

  });

};

//////////////////////////
// Dealing with payload //
//////////////////////////

/**
 * checks whether a payload is valid
 * @param  {Object} payload payload to be transmitted to parcelLab API
 * @param  {Function} callback   (err, isValid)
 */
function checkPayload(payload, callback) {

  var requiredKeys = params.requiredKeys;
  var allowedKeys = requiredKeys.concat(params.allowedKeys);

  if (utils.objHasKeys(payload, requiredKeys)) {
    if (utils.objHasOnlyKeys(payload, allowedKeys)) {

      var okay = true;
      var fail = '';
      var datachecks = params.datachecks;

      for (var i = 0; i < datachecks.email.length; i++) {
        if (!_.isNull(payload[datachecks.email[i]]) && !_.isUndefined(payload[datachecks.email[i]])) {
          okay &= ø.isEmail(payload[datachecks.email[i]]);
        }
      }
      if (!okay) fail = 'Field to be required to be an email is not an email';

      for (var j = 0; j < datachecks.number.length; j++) {
        if (!_.isNull(payload[datachecks.number[j]]) && !_.isUndefined(payload[datachecks.number[j]])) {
          okay &= typeof payload[datachecks.number[j]] === 'number';
        }
      }
      if (!okay) fail = 'Field to be required to be a number is not a number';

      for (var k = 0; k < datachecks.boolean.length; k++) {
        if (!_.isNull(payload[datachecks.boolean[k]]) && !_.isUndefined(payload[datachecks.boolean[k]])) {
          okay &= (typeof payload[datachecks.boolean[k]] === 'boolean');
        }
      }
      if (!okay) fail = 'Field to be required to be a bool is not a bool';

      callback(fail, okay);

    } else callback('Some keys are not allowed', false);
  } else callback('Required keys missing', false);
}

/**
 * Checks whether the payload has multiple tracking numbers in its key
 * @param  {Object}  payload payload to be checked for multieple tracking numbers
 * @return {String}         String for termination of tracking number sequence or null if no multiples
 */
function hasMultipleTrackingNumbers(payload) {
  if (_.isNull(payload.tracking_number)) return null;
  if (_.isObject(payload.tracking_number)) return 'json';
  var terminators = ['|', ','];
  for (var i = 0; i < terminators.length; i++) {
    if (payload.tracking_number.indexOf(terminators[i]) > -1) return terminators[i];
  }
  return null;
}

/**
 * creates an array of payloads out of a single payload with multiples in the tracking_number
 * @param  {Object} payload payload to be multiplied, with a tracking_number like so:
 *                          {ups:["1Z74845R6842887612","1Z74845R6842758029"]}
 * @return {[Object]}         Array of payloads with single tracking numbers
 */
function multiplyOnTrackingNumber(payload) {
  var terminator = hasMultipleTrackingNumbers(payload);
  if (terminator === null) return [payload];

  var tnos = [];
  if (terminator == 'json') {
    var json = payload.tracking_number;
    var jsonCouriers = _.keys(json);
    for (var k = 0; k < jsonCouriers.length; k++) {
      var jsonCourier = guessCourier(jsonCouriers[k]);
      var jsonTnos = json[jsonCouriers[k]];
      for (var l = 0; l < jsonTnos.length; l++) {
        tnos.push({
          courier: jsonCourier,
          tracking_number: jsonTnos[l]
        });
      }
    }

  } else if (terminator.length == 1) {
    var tnos_raw = payload.tracking_number.split(terminator);
    for (var j = 0; j < tnos_raw.length; j++) {
      tnos.push({
        courier: payload.courier,
        tracking_number: tnos_raw[j]
      });
    }
  }

  var payloads = []; // array of new payloads
  for (var i = 0; i < tnos.length; i++) {
    var newPayload = _.extend({}, payload);

    newPayload.courier = tnos[i].courier;
    newPayload.tracking_number = tnos[i].tracking_number;

    payloads.push(newPayload);
  }

  return payloads;
}

/**
 * retrieves courier code from mappings for given courier name if available
 * @param  {String} input name of courier as given by input
 * @return {String}       mapping to actual courier code
 */
function guessCourier(input) {
  var output = input;
  try {
    var knownInputs = _.keys(params.couriers);
    if (knownInputs.indexOf(input.toLowerCase()) > -1) {
      output = params.couriers[input.toLowerCase()];
    }
  } catch (e) {}
  return output;
}

////////////////
// API Access //
////////////////

/**
 * posts a new tracking to the parcelLab API to be tracked
 * @param  {Object}   payload        payload to be transmitted to parcelLab API
 * @param  {Function} callback       (error, result)
 */
function postTrackingToParcelLabAPI(payload, user, token, callback) {

  console.log(payload);

  var url = params.endpoint + 'track/';

  // prepare request
  var options = {
    rejectUnauthorized: false,
    url: url,
    method: 'POST',
    headers: {
      'Content-Type': 'application/json',
      'user': user.toString(),
      'token': token.toString()
    },
    json: true,
    body: payload
  };

  // perform request
  Request(options, function(error, response, body) {
    callback(error, body);
  });

}

////////////
// Export //
////////////

module.exports = API;
