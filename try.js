var parcelLabAPI = require('./index');

var user = 1;
var token = 'parcelLabAPItoken-30characters';

var payload = {
  courier: 'dhl-germany',
  tracking_number: '1234567890|0987654321',
  zip_code: '12345',
  destination_country_iso3: 'DEU',
  deliveryNo: 'dn12345',
  recipient: 'Max Mustermann',
  email: 'info@parcellab.com',
  articleNo: 'an13579',
  articleName: 'Test Artikel',
  cashOnDelivery: 102.3,
  complete: true
};

var payload_embeddedJson = {
  courier: 'xxx',
  tracking_number: {ups:["1Z74845R6842887612","1Z74845R6842758029"], dhl: ["003400012321343"]},
  zip_code: '12345',
  destination_country_iso3: 'DEU',
  deliveryNo: 'dn12345',
  recipient: 'Max Mustermann',
  email: 'info@parcellab.com',
  articleNo: 'an13579',
  articleName: 'Test Artikel'
};

var pl = new parcelLabAPI(user, token);

pl.createTracking(payload, function (err, res) {

  var log = {
    error: err,
    result: res
  };

  console.log(log);

});
